#include "database.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int personacmp(Persona* p1, Persona* p2);
void insert_aux(Database* database, Persona* persona); //given the right position does an insertion into the database

void initStringLeft(IndexNodeString* i, char* value);
void initStringRight(IndexNodeString* i, char* value);
void initIntLeft(IndexNodeInt* i, int value);
void initIntRight(IndexNodeInt* i, int value);


void insert(Database*  database, Persona* persona){
  if(database == NULL || persona == NULL){
    return;
  }
  
  if(persona -> name == NULL || persona -> surname == NULL || persona -> address == NULL){//not accepting partial data
    return;
  }

  if(database -> name == NULL){//no names in database implies no people

    insert_aux(database, persona);

  }
  
  else{//there are other entries in the database
    int done = 0;      
    IndexNodeString* next_node = database -> name;
    IndexNodeString* next_s = database -> surname;
    IndexNodeString* next_a = database -> address;
    IndexNodeInt* next_age = database -> age;
    
    do{

      char* rootname = next_node -> value;
      Persona p = {0};//we find who' s there to find where we need to go
      
      strncpy(p.name , next_node -> value , 20);
      strncpy(p.surname , next_s -> value, 50) ;
      strncpy(p.address , next_a -> value, 100);
      p.age = next_age -> value;
      
      Persona* root = &p;
      
      
      if(personacmp(persona, root) <= 0 ){
        
        //go left
        if(next_node -> left == NULL){
          done = 1; //we found a place to go
          initStringLeft(next_node, persona -> name);
          initStringLeft(next_s, persona -> surname);
          initStringLeft(next_a, persona -> address);
          initIntLeft(next_age, persona -> age);
        
        }
        
        else{
          next_node = next_node -> left; //we look left for a place to go
          next_s = next_s -> left;
          next_a = next_a -> left;
          next_age = next_age -> left;
        }
      }
      
      else{
      
        //go right
        if(next_node -> right == NULL){
          done = 1; //we found a place to go
          initStringRight(next_node, persona -> name);
          initStringRight(next_s, persona -> surname);
          initStringRight(next_a, persona -> address);
          initIntRight(next_age, persona -> age);

        }
        
        else{
          next_node = next_node -> left; //we look right for a place to go
          next_s = next_s -> right;
          next_a = next_a -> right;
          next_age = next_age -> right;
        }
      }
      
    }while(!done);
  }
}


//determines the order of the two structures based on the lexicographic order
//returns 0 if equal, positive integer if p1 > p2, negative if p1 < p2
//why is this needed: mainly for the findByAge function, the age in the database is not a reference to the Persona structure. 
//This means that to get the reference one must get the parallel structure that contains the name, this can be indirectly done if the order is known to begin with.
//there are better solutions (without changing the structure) maybe, I'm too far gone to change now

int personacmp(Persona* p1, Persona* p2){
  int ret = strcmp(p1 -> name, p2 -> name);
  if(ret == 0){
    ret = strcmp(p1 -> surname, p2 -> surname);
    if(ret == 0){
      ret = strcmp(p1 -> address, p2 -> address);
      if(ret == 0){
        ret = p1 -> age - p2 -> age;
        return ret;
      }
      else{
        return  ret;
      }
    }
    else{
      return ret;
    }
  }
  else{
    return ret;
  }
}


//these functions will look only for the first occurence of the given parameter

//the names are always in order (like in a Binary Search Tree) the function is more efficient
Persona* findByName(Database * database, char * name){
  if(database == NULL || name == NULL){
    return NULL; //just to be safe
  }
  
  int ret;
  IndexNodeString* root = database -> name;//start from the top
  
  if(root == NULL){
    return NULL;
  }
  
  ret = strcmp(name, root -> value);
  while(ret != 0){
    if(ret < 0){
    
      //go left
      root = root -> left;
      if(root == NULL){
        printf("person not in database\n");
        return NULL; 
      }
      
      ret = strcmp(name, root -> value);
    }
    else{
    
      //go right
      root = root -> right;
      if(root == NULL){
        printf("person not in database\n");
        return NULL; 
      }
      
      ret = strcmp(name, root -> value);
    }
  }
  return (Persona*) root -> value ;//if i'm correct this should work  
}

//The next parameters to find are not in order by themselves but only with the previous ones (price of lexicographic order) 

Persona* findBySurname(Database * database, char * surname){
  if(database == NULL || surname == NULL){
    return NULL; //just to be safe
  }
  
  //let's do a "first occurence" tree-traversal, we return the first instance of persona with right surname
  
  IndexNodeString* next_node = database -> name;
  IndexNodeString* next_s = database -> surname;
  IndexNodeString* next_a = database -> address;
  IndexNodeInt* next_age = database -> age;
  
  if(next_node == NULL){//if one is null so are the others
    return NULL; 
  } 
  
  if(strcmp(surname, next_s -> value) == 0){//found it
    return (Persona*) next_node -> value;
  }
  
  Database left = {.name = next_node -> left, .surname = next_s -> left, .address = next_a -> left, .age = next_age -> left};
  
  Database right = {.name = next_node -> right, .surname = next_s -> right, .address = next_a -> right, .age = next_age -> right};
  
  Persona* p;
  
  p = findBySurname(&left, surname);
  if(p != NULL){
    return p;
  }
  
  p = findBySurname(&right, surname);
  if(p != NULL){
    return p;
  }
  
  return NULL;
}

Persona* findByAddress(Database * database, char * address){
  if(database == NULL || address == NULL){
    return NULL; //just to be safe
  }
  
  //let's do a "first occurence" tree-traversal, we return the first instance of persona with right address
  
  IndexNodeString* next_node = database -> name;
  IndexNodeString* next_s = database -> surname;
  IndexNodeString* next_a = database -> address;
  IndexNodeInt* next_age = database -> age;
  
  if(next_node == NULL){//if one is null so are the others
    return NULL; 
  } 
  
  if(strcmp(address, next_a -> value) == 0){//found it
    return (Persona*) next_node -> value;
  }
  
  Database left = {.name = next_node -> left, .surname = next_s -> left, .address = next_a -> left, .age = next_age -> left};
  
  Database right = {.name = next_node -> right, .surname = next_s -> right, .address = next_a -> right, .age = next_age -> right};
  
  Persona* p;
  
  p = findByAddress(&left, address);
  if(p != NULL){
    return p;
  }
  
  p = findByAddress(&right, address);
  if(p != NULL){
    return p;
  }
  
  return NULL;
}


Persona* findByAge(Database * database, int age){
  if(database == NULL){
    return NULL; //just to be safe
  }
  
  //let's do a "first occurence" tree-traversal, we return the first instance of persona with right age
  
  IndexNodeString* next_node = database -> name;
  IndexNodeString* next_s = database -> surname;
  IndexNodeString* next_a = database -> address;
  IndexNodeInt* next_age = database -> age;
  
  if(next_node == NULL){//if one is null so are the others
    return NULL; 
  } 
  
  if(next_age -> value == age){//found it
    return (Persona*) next_node -> value;
  }
  
  Database left = {.name = next_node -> left, .surname = next_s -> left, .address = next_a -> left, .age = next_age -> left};
  
  Database right = {.name = next_node -> right, .surname = next_s -> right, .address = next_a -> right, .age = next_age -> right};
  
  Persona* p;
  
  p = findByAge(&left, age);
  if(p != NULL){
    return p;
  }
  
  p = findByAge(&right, age);
  if(p != NULL){
    return p;
  }
  
  return NULL;
}

void freeDatabase(Database* d){
  if(d == NULL){
    return;
  }
  freeIndexNodeString(d -> name);
  freeIndexNodeString(d -> surname);
  freeIndexNodeString(d -> address);
  freeIndexNodeInt(d -> age);
}
void freeIndexNodeString(IndexNodeString* i){
  if(i == NULL){
    return;
  }
  freeIndexNodeString(i -> left);
  freeIndexNodeString(i -> right);
  free(i);
}
void freeIndexNodeInt(IndexNodeInt* i){
  if(i == NULL){
    return;
  }
  freeIndexNodeInt(i -> left);
  freeIndexNodeInt(i -> right);
  free(i);
}

void insert_aux(Database* database, Persona* persona){
  
  database -> name = malloc(sizeof(IndexNodeString));
    
  if(database -> name == NULL){
    printf("malloc failed\n");
    exit(-1);
  }
    
  database -> name -> value = persona -> name;
  database -> name -> left = NULL;
  database -> name -> right = NULL;
  
  database -> surname = malloc(sizeof(IndexNodeString));
  
  if(database -> surname == NULL){
    printf("malloc failed\n");
    exit(-1);
  }
    
  database -> surname -> value = persona -> surname;
  database -> surname -> left = NULL;
  database -> surname -> right = NULL;
    
  database -> address = malloc(sizeof(IndexNodeString));
    
  if(database -> address == NULL){
    printf("malloc failed\n");
    exit(-1);
  }
  
  database -> address -> value = persona -> address;
  database -> address -> left = NULL;
  database -> address -> right = NULL;
    
  database -> age = malloc(sizeof(IndexNodeInt));
    
  if(database -> age == NULL){
    printf("malloc failed\n");
    exit(-1);
  }
    
  database -> age -> value = persona -> age;
  database -> age -> left = NULL;
  database -> age -> right = NULL;
}

void initStringLeft(IndexNodeString* i, char* value){
  i -> left = malloc(sizeof(IndexNodeString));
  if(i -> left == NULL){
    printf("malloc failed\n");
    exit(-1);
  }
  
  i -> left -> value = value;
  i -> left -> left = NULL;
  i -> left -> right = NULL;
  
}
void initStringRight(IndexNodeString* i, char* value){
  i -> right = malloc(sizeof(IndexNodeString));
  if(i -> right == NULL){
    printf("malloc failed\n");
    exit(-1);
  }
  
  i -> right -> value = value;
  i -> right -> left = NULL;
  i -> right -> right = NULL;
  
}
void initIntLeft(IndexNodeInt* i, int value){
  i -> left = malloc(sizeof(IndexNodeInt));
  if(i -> left == NULL){
    printf("malloc failed\n");
    exit(-1);
  }
  
  i -> left -> value = value;
  i -> left -> left = NULL;
  i -> left -> right = NULL;

}
void initIntRight(IndexNodeInt* i, int value){
  i -> right = malloc(sizeof(IndexNodeInt));
  if(i -> right == NULL){
    printf("malloc failed\n");
    exit(-1);
  }
  
  i -> right -> value = value;
  i -> right -> left = NULL;
  i -> right -> right = NULL;

}