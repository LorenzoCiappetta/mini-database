#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "database.h"

void printsona(Persona* persona);
void printInOrder(Database* d);

int main(){
  Persona* p1 = malloc(sizeof(Persona));
  strncpy(p1 -> name , "Mario" , 20);
  strncpy(p1 -> surname , "Mario", 50) ;
  strncpy(p1 -> address , "New York", 100);
  p1 -> age = 999;
  
  //printsona(p1);
  
  Persona* p2 = malloc(sizeof(Persona));
  strncpy(p2 -> name, "Luigi", 20);
  strncpy(p2 -> surname , "Mario", 50);
  strncpy(p2 -> address , "New York", 100);
  p2 -> age = 888; 
  
  Persona* p3 = malloc(sizeof(Persona));
  strncpy(p3 -> name, "Aaaaaaaaa", 20);
  strncpy(p3 -> surname, "aaaaaaaa", 50);
  strncpy(p3 -> address , "aaaaaaaaa", 100);
  p3 -> age = 1;
  
  Persona* p4 = malloc(sizeof(Persona));
  strncpy(p4 -> name , "Bbbbbbbbbbbb", 20);
  strncpy(p4 -> surname , "bbbbbbbbb", 50);
  strncpy(p4 -> address , "bbbbbbbb", 100);
  p4 -> age = 2;
  
  Database d = {0};
  
  insert(&d, p1);
  
  insert(&d, p2);
  
  insert(&d, p3);
  
  insert(&d, p4);

  printsona(findByName(&d,"Mario"));

  printsona(findByName(&d,"Luigi"));
  
  printsona(findByName(&d,"Aaaaaaaaa"));
  
  printsona(findByName(&d,"Bbbbbbbbbbbb"));
  
  printsona(findByName(&d,"mimmo"));
    
  printsona(findBySurname(&d,"Mario"));

  printsona(findBySurname(&d,"Dario"));
  
  printsona(findBySurname(&d,"aaaaaaaa"));
    
  printsona(findBySurname(&d,"bbbbbbbbb"));
  
  printsona(findBySurname(&d,"mimmo"));
  
  printsona(findByAddress(&d,"New York"));
  
  printsona(findByAddress(&d,"New York"));
  
  printsona(findByAddress(&d,"aaaaaaaaa"));
  
  printsona(findByAddress(&d,"bbbbbbbb"));
  
  printsona(findByAddress(&d,"mimmo"));
  
  printsona(findByAge(&d,999));
  
  printInOrder(&d);
  freeDatabase(&d);
  free(p1);
  free(p2);
  free(p3);
  free(p4);
}

void printsona(Persona* persona){
  if(persona == NULL){
    printf("persona not found\n");
    return;
  }
  printf("%s %s %s %d\n", persona -> name, persona -> surname, persona -> address, persona -> age);
}

void printInOrder(Database* d){
  if(d == NULL){
    return;
  }
  
  IndexNodeString* next_node = d -> name;
  IndexNodeString* next_s = d -> surname;
  IndexNodeString* next_a = d -> address;
  IndexNodeInt* next_age = d -> age;
  
  if(next_node == NULL){
    return;
  } 
  
  Persona p = {0};//we find who' s there to find where we need to go
  
  strncpy(p.name , next_node -> value , 20);
  strncpy(p.surname , next_s -> value, 50) ;
  strncpy(p.address , next_a -> value, 100);
  p.age = next_age -> value;
      
  Persona* root = &p;
  
  Database left = {.name = next_node -> left, .surname = next_s -> left, .address = next_a -> left, .age = next_age -> left};
  
  Database right = {.name = next_node -> right, .surname = next_s -> right, .address = next_a -> right, .age = next_age -> right};
  
  printInOrder(&left);
  printsona(root);
  printInOrder(&right);
  
}


